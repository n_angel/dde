CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))
BINDIR := bin/
OBJDIR := obj/
SRCDIR := src/
CFLAGS = -std=c++11 -O3

main: $(OBJ_FILES)
			mkdir -p $(BINDIR)
			g++ $(CFLAGS) -o $(BINDIR)$@ $^ -lm

obj/%.o: src/%.cpp
			mkdir -p $(OBJDIR)
			g++ $(CFLAGS) -c -o $@ $< -lm
			
optimized:
	make -f makefile CFLAGS="-O3"

debug: 
	make -f makefile CFLAGS="-g"

clean:
	rm -f $(OBJ_FILES) 
	rm -f $(BINDIR)*~
	rm -f $(OBJDIR)*~
	rm -f $(SRCDIR)*~
	
cleanall:
	rm -f $(OBJ_FILES) $(BINDIR)main
	rm -fr $(BINDIR)
	rm -fr $(OBJDIR)

run:
	./bin/main -D 10 -FN 1 -itrs 30000 -spop 50 -CR 0.9 -fLow 0.1 -fUp 0.1 -fPrb 1.0 -mt DIV -div_mth BEST -theta 0.9 -itprint 100 -seed 0 -outfile outs/D10_FN1_itrs30000_spop50_CR0.9_fLow0.1_fUp0.1_fPrb1.0_mtDIV_divmthBEST_theta0.9_itprint100_seed0.txt
