/*
 * DifferentialEvolution.h
 *
 * Implementation of DifferentialEvolution.h
 */

# include "DifferentialEvolution.h"

 void test_func(double *, double *,int,int,int, std::string);

double *OShift,*M,*y,*z,*x_bound;
int ini_flag=0,n_flag,func_flag;

//// Used namespaces. ///////////////////////////////////////////////////////


/* 
* \brief  Contructor de la clase
*/
DE::DE(int problem, int dimension, int size_population, double seed, std::string path)
{
  this->path = path;

  this->generator = std::default_random_engine(seed);

  this->problem = problem;
  this->dimension = dimension;
  this->size_population = size_population;

  this->initializeFitnessFunctionParameters(  );
  this->initialize_population(  );

  // For Diversity Scheme
  this->DI = this->calculate_DI(  );
}

void DE::initialize_population(  )
{
  //(max - min) * ( (double)rand() / (double)RAND_MAX ) + min
  //this->population = std::vector< std::vector< double > >(this->size_population, std::vector< double >(this->dimension, 0));
  //this->f = std::vector< double >(this->size_population);
  this->population = std::vector< individual >(this->size_population);
  this->best.fitness = std::numeric_limits<double>::max();
  double rnd;

  //std::uniform_real_distribution<double> distribution(LOWER_BOUND, UPPER_BOUND);
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  
  for (int it_i = 0; it_i < this->size_population; ++it_i)
  {
    this->population[it_i].x = std::vector<double> (this->dimension, 0);
    /*
    for (int it_j = 0; it_j < this->dimension; ++it_j)
    {
      //this->population[it_i].x[it_j] = (UPPER_BOUND - LOWER_BOUND) * ( (double)rand() / (double)RAND_MAX ) + LOWER_BOUND;
      this->population[it_i].x[it_j] = distribution(this->generator);
    }*/
    for (int it_j = 0; it_j < this->dimension; ++it_j)
    {
      rnd = distribution(this->generator);
      this->population[it_i].x[it_j] = LOWER_BOUND + rnd * (UPPER_BOUND - LOWER_BOUND);
    }

    test_func(&this->population[it_i].x.front(),  &this->population[it_i].fitness, this->dimension, 1, this->problem, this->path);

    // Ajuste del fitness
    if ((this->population[it_i].fitness - optimum) < epsilon) {
      this->population[it_i].fitness = optimum;
    }

    // Conservar la mejor solucion
    if (this->population[it_i].fitness < this->best.fitness)
    {
      this->best = this->population[it_i];
    }
  }
}

void DE::initializeFitnessFunctionParameters()
{
  //epsilon is acceptable error value
  this->epsilon = pow(10.0, -8);

  //set optimal value
  switch(this->problem) {
  case 1 :
    this->optimum = -1400;
    break;
  case 2 :
    this->optimum = -1300;
    break;
  case 3 :
    this->optimum = -1200;
    break;
  case 4 :
    this->optimum = -1100;
    break;
  case 5 :
    this->optimum = -1000;
    break;
  case 6 :
    this->optimum = -900;
    break;
  case 7 :
    this->optimum = -800;
    break;
  case 8 :
    this->optimum = -700;
    break;
  case 9 :
    this->optimum = -600;
    break;
  case 10 :
    this->optimum = -500;
    break;
  case 11 :
    this->optimum = -400;
    break;
  case 12 :
    this->optimum = -300;
    break;
  case 13 :
    this->optimum = -200;
    break;
  case 14 :
    this->optimum = -100;
    break;
  case 15 :
    this->optimum = 100;
    break;
  case 16 :
    this->optimum = 200;
    break;
  case 17 :
    this->optimum = 300;
    break;
  case 18 :
    this->optimum = 400;
    break;
  case 19 :
    this->optimum = 500;
    break;
  case 20 :
    this->optimum = 600;
    break;
  case 21 :
    this->optimum = 700;
    break;
  case 22 :
    this->optimum = 800;
    break;
  case 23 :
    this->optimum = 900;
    break;
  case 24 :
    this->optimum = 1000;
    break;
  case 25 :
    this->optimum = 1100;
    break;
  case 26 :
    this->optimum = 1200;
    break;
  case 27 :
    this->optimum = 1300;
    break;
  case 28 :
    this->optimum = 1400;
    break;
  }
}


void DE::minimize(int iterations, double CR, STEP F, SCTMTH sl_method, OUTFILE keeper)
{
  int r0, r1, r2, jrand;
  int itrt = 0;
  double fStep;
  std::vector< individual > u;
  this->T_elapsed = 1;
  this->T_end = iterations;
  this->sl_method = sl_method;

  std::uniform_int_distribution<int> distribution_r(0, this->size_population-1);
  std::uniform_int_distribution<int> distribution_d(0, this->dimension-1);
  std::uniform_real_distribution<double> distribution_0To1(0.0, 1.0);

  do
  {
    u = std::vector< individual >(this->size_population);
    for (int i = 0; i < this->size_population; ++i) // Para cada elemento de la poblacion
    {
      u[i].x = std::vector<double>(this->dimension, 0.0);
      do{ r0 = distribution_r(this->generator); } while(r0 == i);
      do{ r1 = distribution_r(this->generator); } while(r1 == r0 || r1==i);
      do{ r2 = distribution_r(this->generator); } while(r2 == r1 || r2==r0 ||r2==i);
      jrand = distribution_d(this->generator);

      for (int j = 0; j < this->dimension; ++j) // Para cada elemento del individuo
      {
        if (distribution_0To1(this->generator)<=CR || j==jrand)
        {
          fStep = (distribution_0To1(this->generator) <= F.F_prb)? F.F_low : F.F_up;
          u[i].x[j] = this->check_Constraints(this->population[r0].x[j], (fStep * (this->population[r1].x[j] - this->population[r2].x[j])));
        }
        else
        {
          u[i].x[j] = this->population[i].x[j];
        }
      }
    }

    this->selection(u.begin());
    if ((int)this->T_elapsed % (int)keeper.iterations == 0)
    {
      this->print_information(keeper.filename);
    }
    this->T_elapsed ++;
  }while(this->T_elapsed <= this->T_end);
}

double DE::check_Constraints(double x_ij, double inc)
{
  if (x_ij + inc < LOWER_BOUND)
  {
    std::uniform_real_distribution<double> distribution_adjustment(LOWER_BOUND, x_ij);
    return distribution_adjustment(this->generator);
  }
  else if (x_ij + inc > UPPER_BOUND)
  {
    std::uniform_real_distribution<double> distribution_adjustment(x_ij, UPPER_BOUND);
    return distribution_adjustment(this->generator);
  }

  return x_ij + inc;
}

void DE::selection( std::vector< individual >::iterator u)
{
  if (this->sl_method.method == ELITISM)  // "Elitism"
  {
    this->elitism(u);
  }
  else if (this->sl_method.method == DIV) // "Div"
  {
    this->div(u);
  }
  else
  {
    std::cout << "Metodo de seleccion desconocido" << std::endl;
    exit(-1); 
  }
}

void DE::elitism(std::vector< individual >::iterator u)
{
  for (int i = 0; i < this->size_population; ++i, u++) // Para cada elemento de la poblacion
  {
    test_func(&u->x.front(),  &u->fitness, this->dimension, 1, this->problem, this->path);

    if ((u->fitness - optimum) < epsilon) {
      u->fitness = optimum;
    }

    // Conservar el nuevo individuo o padre
    if (u->fitness <= this->population[i].fitness)
    {
      this->population[i].x = u->x;
      this->population[i].fitness = u->fitness;
    }

    // Conservar la mejor solucion
    if (this->population[i].fitness < this->best.fitness)
    {
      this->best = this->population[i];
    }
  }
}

/*
* \brief  Calculate 
*/
double DE::calculate_DI()
{
  double N = this->dimension;
  double I = this->size_population;

  /*
  * We want to get the radio of the I hyperspheres that cover whole the hypervolume of the search space. 
  * R where Hh(R) * I = He
  * Where:
  *   He = Search space Hypervolume = (R-L)^N
  *   Hh = Hypervolume with radio R (https://es.wikipedia.org/wiki/N-esfera)
  *   I = Total of individuals
  *
  * R = n-root((He * gamma(n/2+1)) / (I * PI^(n/2)))
  *   R Upper bound (Dominio)
  *   L Lower bound (Dominio)
  *   N Dimension of the problem (Number of variables)
  *   gamma function = gamma(t) = (t-1)!
  */
  double He = pow(UPPER_BOUND - LOWER_BOUND, N); 
  double gm = EXFUN::Legendre(N/2+1); //gamma(n/2+1), gamma function proposed by Adrien-Marie Legendre

  double aux = (He * gm) / (I * pow(M_PI, N/2)); 
  double R = pow(aux, 1.0/N);
  return R;
}

void DE::div(std::vector< individual >::iterator u)
{
  int max_DCN_id;

  //Current_members = Population u Offspring 
  std::vector< individual > Current_members = this->population;
  std::vector< individual >::iterator itrt = u;
  for (int it = 0; it < this->size_population; ++it, itrt++)
  {
    test_func(&itrt->x.front(),  &itrt->fitness, this->dimension, 1, this->problem, this->path);  // Offspring Fitness

    if ((itrt->fitness - optimum) < epsilon)
    {
      itrt->fitness = optimum;
    }

    Current_members.push_back(*itrt); // Join
  }

  // Best = Individual with best f(x) in CurrentMembers
  int best_id = this->Best_Individual( Current_members.begin(), Current_members.end() );
  this->best = Current_members[best_id];

  // NewPop = { Best }
  this->population.clear();
  this->population.push_back(Current_members[best_id]);

  // CurrentMembers = CurrentMembers - { Best }
  Current_members.erase(Current_members.begin() + best_id);

  int last_new_element;
  while((int)this->population.size() < this->size_population)
  {
    last_new_element = (int)this->population.size() - 1;

    //Get the distance for each element in CurrentMembers  respect to each element in the new population
    this->DistanceClosestNeighbor(Current_members.begin(), Current_members.end(), this->population[last_new_element]);

    // minimum DCN required to avoid being penalized.
    this->D = this->DI - (this->DI * (this->T_elapsed / (this->T_end * this->sl_method.theta)));
    this->D = (this->D < 0)? 0 : this->D;
    //this->D = 0;
    
    // Penalize(CurrentMembers, D)
    this->penalize(Current_members.begin(  ), Current_members.end(  ), D);
    std::vector< individual > F;

    // ND = Non-dominated individuals of CurrentMembers (without repetitions)
    F = PARETO::Get_Front(Current_members.begin(  ), Current_members.end(  ), D);

    // Selected = Randomly select an individual from ND
    this->front_size = (int)F.size(  );
    int rand_id = this->front_size - 1; // Default: Keep best individual on Pareto Front, the lower fitness was the last to put in the front
    if (this->sl_method.sub_method == "RAND")
    {
      rand_id = 0;
      if (this->front_size > 1)  // Get an element in the percentage of the population of the desired Pareto Front
      {
        // The percentage of population with the lowest DCN (choose pop_pctg back to front)
        int elements_in_range = (this->front_size * (1.0 - this->sl_method.pop_pctg)) + 0.5; // add 0.5 to get up value check case 2 with pop_pctg 0.4
        std::uniform_int_distribution<int> distribution_rand_front_element(elements_in_range, this->front_size-1);
        rand_id = distribution_rand_front_element(this->generator);
      }
    }

    // NewPop = NewPop u Selected
    individual nw;
    nw.x = Current_members[F[rand_id].id].x;
    nw.fitness = nw.fx = Current_members[F[rand_id].id].fitness;
    
    /*
    test_func(&nw.x.front(),  &nw.fitness, this->dimension, 1, this->problem, this->path);
    // Ajuste del fitness
    if ((nw.fitness - optimum) < epsilon) {
      nw.fitness = optimum;
    }

    if (old != nw.fitness)
    {
      std::cout << "Error diferencia en Fitness" << std::endl;
      exit(-1);
    }
    */

    // Keep the best indiivdual
    this->population.push_back(nw);
    if (nw.fitness < this->best.fitness)
    {
      this->best = nw;
    }

    // CurrentMembers = CurrentMembers - { Selected }
    Current_members.erase(Current_members.begin() + F[rand_id].id);
  }
}


/*
* \brief  Get the id of the best individual
* \param  Iterator Population
*/
int DE::Best_Individual( std::vector< individual >::iterator it_begin, std::vector< individual >::iterator it_end )
{
  double best = it_begin->fitness;
  double best_id = 0;
  int it = 1;
  it_begin->dcn = std::numeric_limits<double>::max();
  it_begin->fx = it_begin->fitness;
  it_begin ++;
  
  while(it_begin != it_end)
  {
    if (it_begin->fitness < best)
    {
      best = it_begin->fitness;
      best_id = it;
    }

    // Auxiliares para el calculo del frente al hacer el problema multiobjetivo (Fitness, DCN)
    it_begin->dcn = std::numeric_limits<double>::max();
    it_begin->fx = it_begin->fitness;

    it ++;
    it_begin ++;
  }

  return best_id;
}

/*
* \brief  Get the vector with distance to the new individual
* \param  current_begin Iterator of the current population begin
* \param  current_end Iterator of the current population end
* \param  new_element new element in the population set
*/
void DE::DistanceClosestNeighbor
(
  std::vector<individual>::iterator current_begin,
  std::vector<individual>::iterator current_end,
  individual new_element
)
{
  double dist;

  for (std::vector<individual>::iterator it_current = current_begin; it_current != current_end ; it_current ++) // each element in Current population
  {
      dist = EXFUN::Euclidean_Distance( it_current->x, new_element.x);
      if (dist < it_current->dcn)
      {
        it_current->dcn = dist;
      }
      
      it_current->fx = it_current->fitness;
  }
}

/*
* \brief Penaliza respecto a al valor del dcn y D, retorna el numero de elemntos penalizados
* Hace uso de la estructura mobj, en donde f1 = DCN y f2 = fitness del individuo
* \param  DCN_begin Iterador apunta al inicio del vector de distancias
* Retorna el elemento con mayor dcn del conjunto poblacional, esto para el caso en que todos los elementos so penalizados
*/
void DE::penalize(std::vector< individual >::iterator DCN_begin, std::vector< individual >::iterator DCN_end, double D)
{
 while(DCN_begin != DCN_end)
  {
    if (DCN_begin->dcn <= D)
    {
      DCN_begin->fx = std::numeric_limits<double>::max();
    }

    DCN_begin ++;
  }
}

void DE::print_information(std::string filename)
{
  std::ofstream myfile;
  myfile.open (filename, std::fstream::out | std::fstream::app);

  myfile << "\nIteration: " << this->T_elapsed << std::endl;
  myfile << "PF size: " << this->front_size << std::endl;
  myfile << std::setprecision(9) << std::fixed;
  myfile << "D penalized: " << this->D << std::endl;
  myfile << "Best: ";
  for (int it_i = 0; it_i < this->dimension; ++it_i)
  {
    myfile << this->best.x[it_i] << " ";
  }
  myfile << this->best.fitness << std::endl;

  //myfile << "Relative Error: " << abs(this->best.fitness - this->optimum) / this->optimum << std::endl;
  myfile << "Absolute Error: " << this->best.fitness - this->optimum << std::endl;
  myfile << "Population: " << std::endl;
  for (int it_i = 0; it_i < this->size_population; ++it_i)
  {
    for (int it_j = 0; it_j < this->dimension; ++it_j)
    {
      myfile << this->population[it_i].x[it_j] << " ";
    }
    myfile << this->population[it_i].fitness << std::endl;
  }

  myfile.close();

  // Error relativo
  //std::cout << "iteracion: " << this->T_elapsed << "\nfitness: " << this->best.fitness << "\n";
  //std::cout << "Error: " << (this->optimum - this->best.fitness) / this->optimum << "\n" << std::endl;
}


