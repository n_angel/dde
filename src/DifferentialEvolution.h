/*
 * DifferentialEvolution.h
 *
 */


#ifndef DIFFERENTIAL_EVOLUTION_H
#define DIFFERENTIAL_EVOLUTION_H


//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <iostream>
# include <climits>
# include <cstddef>
# include <cassert>
# include <random>
# include <stdlib.h>
# include <climits>
# include <fstream> 
# include <random>
 #include <iomanip> 

//// New includes. /////////////////////////////////////////////////////
# include "parameter_handler.h"
# include "random.h"
# include "pareto_front.h"
# include "io_structures.h"

# include "extra_functions.h"

# include "global.h"
# include "pareto_front.h"



//// Class Definitions		////////////////////////////////////////////////
class DE
{
	public:
		DE(int problem, int dimension, int size_population, double seed, std::string path);
		void initialize_population(  );
		void minimize(int iterations, double CR, STEP F, SCTMTH sl_method, OUTFILE keeper);
		void print_information(std::string filename);
		
		/*int best_individual( );
		std::vector<int> penalize(std::vector<double> DCN, double D);
		std::vector<double> DistanceClosestNeighbor
			(
			  std::vector< std::vector< double > >::iterator newPop, 
			  int size_newPop, 
			  double *DI
			);
			*/

	private:
		// Vars
		std::default_random_engine generator;

		int problem = func_code["SPHERE_FUNC"];
		int dimension = 10;
		int size_population = 20;

		std::string path;

		double epsilon;
		double optimum;

		individual best;

		double DI;
		double D;

		SCTMTH sl_method;
		int front_size;

		std::vector< individual > population;
		//std::vector< double > f;
		//std::vector< std::vector<double> > u;

		double T_elapsed;
		double T_end;

		void initializeFitnessFunctionParameters(  );

		double check_Constraints(double x_ij, double inc);

		void selection(std::vector< individual >::iterator u);
		void elitism(std::vector< individual >::iterator u);
		void div(std::vector< individual >::iterator u);

		int Best_Individual( std::vector< individual >::iterator it_begin, std::vector< individual >::iterator it_end );
		void DistanceClosestNeighbor
			(
			  std::vector<individual>::iterator current_begin,
			  std::vector<individual>::iterator current_end,
			  individual new_element
			);
		double calculate_DI(  );
		void penalize(std::vector< individual >::iterator DCN_begin, std::vector< individual >::iterator DCN_end, double D);
		
};  // Differential Evolution namespace

//}  // Algorithm namespace

#endif