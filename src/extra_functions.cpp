
# include "extra_functions.h"

double EXFUN::fact(double n)
{
	if(n == 1)
	{
		return 1;
	}

	return n * fact(n-1);
}

double EXFUN::pw(double base, double exp)
{
	if((int)exp == 0)
	{
		return 1;
	}

	return base * pw(base, exp-1);
}


double EXFUN::Legendre(double n)
{
	return fact(n-1);
}

/*
*	\param	n	Hypersphere dimension 
*	\param 	r 	Hypersphere Radio
*/
double EXFUN::Hypersphere_Volume(double n, double R, double (*gamma)(double))
{
	//std::cout << "pi: " << pow(M_PI, n/2) << std::endl;
	//std::cout << "pow: " << pow(R, n) << std::endl;
	return (pow(M_PI, n/2) * pow(R, n)) / gamma(n/2+1);
}

double EXFUN::Euclidean_Distance( std::vector<double> I1, std::vector<double> I2)
{
  int size_indv = (int)I1.size(  );
  double dst = 0.0;
  for (int i = 0; i < size_indv; ++i)
  {
    dst += (I1[i] - I2[i]) * (I1[i] - I2[i]);
  }

  return sqrt(dst);
}