
#ifndef EXTRA_FUNC_H
#define EXTRA_FUNC_H

# include <vector>
# include <string>
# include <algorithm>
# include <math.h>
# include <iostream>

namespace EXFUN
{
	double fact(double n);
	double pw(double base, double exp);
	double Legendre(double n);
	double Hypersphere_Volume(double n, double R, double (*gamma)(double));

	double Euclidean_Distance( std::vector<double> I1, std::vector<double> I2);
}

#endif