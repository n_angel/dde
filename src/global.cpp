# include "global.h"

 /* 
Funcion auxiliar para el ordenamiento de la población
*/
bool sort_by_objetives(individual A, individual B)
{
	if (A.dcn == B.dcn)
	{
		return A.fx < B.fx;
	}	
	
	return A.dcn > B.dcn;
} 


void ERROR(std::string message)
{
	std::cout << message << std::endl;
	exit(-1);
}