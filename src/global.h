#ifndef GLOBAL_H
#define GLOBAL_H


# include <vector>
# include <iostream>
# include <climits>
# include <cstddef>


// Search range: [-100,100]
# define LOWER_BOUND	-100.0
# define UPPER_BOUND	100.0

# define ELITISM "ELITISM"
# define DIV "DIV"

//// Standard includes. /////////////////////////////////////////////////////

 typedef struct INDIVIDUAL
 {
 	std::vector<double> x;
 	double fitness = 0.0;
 	double fx;
 	double dcn;
 	int id;
 }individual;

/*
 typedef struct MOBJ
 {
 	double dcn;
 	double fx;
 	int id;
 } mobj;
*/

 typedef struct selection_method
 {
 	std::string method; // {ELITISM, DIV}
 	std::string sub_method; // DIV: {BEST, RAND}
 	double pop_pctg;	// To rand selection 
 	double theta;	// Falling speed of the penalty limit D
 }SCTMTH;

 typedef struct out_file
 {
 	std::string filename;
 	double iterations;
 }OUTFILE;

 typedef struct step
 {
 	double F_low;
 	double F_up;
 	double F_prb; // Probability to choose F_low
 }STEP;

bool sort_by_objetives(individual A, individual B);
void ERROR(std::string message);
 
#endif