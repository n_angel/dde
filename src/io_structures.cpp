
/*
 * ExampleProblems.h
 *
 * Implementation of ExampleProblems.h.
 */

#include "io_structures.h"

void Print_Structure_mobj
(
  std::vector< individual >::iterator points_begin, 
  std::vector< individual >::iterator points_end, 
  std::string filename
)
{
  std::ofstream myfile;
  myfile.open (filename);
  while(points_begin != points_end)
  {
    myfile << points_begin->dcn << " " << points_begin->fx << std::endl;
    points_begin ++;
  }
  myfile.close();
}

/*
* \brief  Print individual information 
*/
void Print_Structure_individual
(
  std::vector< individual >::iterator points_begin, 
  std::vector< individual >::iterator points_end, 
  std::string filename,
  bool append_flag
)
{
  std::ofstream myfile;
  if (append_flag == false)
  {
    myfile.open (filename);
  }
  else
  {
    myfile.open (filename, std::ofstream::out | std::ofstream::app);
  }
  
  while(points_begin != points_end)
  {
    for (int it_i = 0; it_i < (int)points_begin->x.size(  ); ++it_i)
    {
      myfile << points_begin->x[it_i] << " ";
    }
    myfile << points_begin->fitness << std::endl;
    points_begin ++;
  }
  myfile.close();
}
 
/*
* \brief  Print individual information of multi objetive structure
*/
void Print_Structure_mobj
(
  std::vector< individual >::iterator points_begin, 
  std::vector< individual >::iterator points_end
)
{
  while(points_begin != points_end)
  {
    std::cout << points_begin->dcn << " " << points_begin->fx << std::endl;
    points_begin ++;
  }
}