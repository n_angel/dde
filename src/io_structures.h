
/*
 * ExampleProblems.h
 *
 * Defines the problems described in the EMO 2005 paper, namely WFG1--WFG9
 * and I1--I5. For the specifics of each problem, refer to the EMO 2005 paper
 * (available from the WFG web site).
 */


#ifndef IO_STRUCTURES_H
#define IO_STRUCTURES_H


//// Standard includes. /////////////////////////////////////////////////////

# include <cassert>
# include <math.h>
# include <vector>
# include <iostream>
# include <iterator>
# include <stdexcept>  
# include <fstream> 

# include "global.h"

//// Definitions/namespaces. ////////////////////////////////////////////////

void Print_Structure_mobj
(
  std::vector< individual >::iterator points_begin, 
  std::vector< individual >::iterator points_end, 
  std::string filename
);

void Print_Structure_mobj
(
  std::vector< individual >::iterator points_begin, 
  std::vector< individual >::iterator points_end
);

#endif