/*
  CEC13 Test function suite 
  Jane Jing Liang (email: liangjing@zzu.edu.cn) 
  Dec. 23th 2012 
*/


//#include <WINDOWS.H>    
# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <map>

# include "parameter_handler.h"
# include "DifferentialEvolution.h"



int main(int argc, char *argv[])
{
	std::cout << "Diversity Differential Evolution Algorithm, Version 1.1" << std::endl;
	std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);

	// Parameters definition
	// Dimension
	int dimension = 10;
	if (params.find("-D") != params.end())
	{
		dimension = std::stoi(params.find("-D")->second);	// n in the algorithm
		if (dimension < 2) ERROR("Error! in the range of parameters, the dimension is not valid");
	}

	// Crossover probability
	double CR = 0.9;
	if (params.find("-CR") != params.end())
	{
		CR = std::stod(params.find("-CR")->second);	// Probabilidad de cruce de un individuo
		if (CR < 0) ERROR("Error! in the range of parameters -CR must be greater or equal than 0");
	}

	// Step size
	STEP F;
	F.F_low = F.F_up = 0.9;
	if (params.find("-fLow") != params.end())
	{
		F.F_up = F.F_low = std::stod(params.find("-fLow")->second);	// Step size (low F)
	}
	if (params.find("-fUp") != params.end())
	{
		F.F_up = std::stod(params.find("-fUp")->second);	// Step size (Upper F)
	}
	if (F.F_up < F.F_low) ERROR("Error! in the range of parameters -fLow must be less or equal to than -fUp");

	// Step size probability
	F.F_prb = 1.0;
	if (params.find("-fPrb") != params.end())
	{
		F.F_prb = std::stod(params.find("-fPrb")->second);	// Probability of choosing the F-low as step size
		if (F.F_prb <= 0) ERROR("Error! in the range of parameters -fPrb must be greater than 0");
	}

	// Random Seed
	double seed = 1.0;
	if (params.find("-seed") != params.end())
	{
		seed = std::stod(params.find("-seed")->second);	// Probability of choosing the F-low as step size
	}

	// Size population
	int size_population = 10;
	if (params.find("-spop") != params.end())
	{
		size_population = std::stoi(params.find("-spop")->second);	// Population size
		if (size_population < 5) ERROR("Error! in the range of parameters -spop must be greater or equal than 5");
	}

	// Iterations
	int iterations = 1000;
	if (params.find("-itrs") != params.end())
	{
		iterations = std::stoi(params.find("-itrs")->second);	// iterations
		if (iterations < 1) ERROR("Error! in the range of parameters -itrs must be greater or equal than 1");
	}

	int problem = 1;
	if (params.find("-FN") != params.end())
	{
		problem = std::stoi(params.find("-FN")->second);	// Function number
	}

	SCTMTH sl_method;
	sl_method.method = "ELITISM";	//	{ELITISM, DIV}
	sl_method.sub_method = "";	//	{BEST, RAND}
	sl_method.pop_pctg = -1.0;	// Percentage of Pareto Front for random selection
	sl_method.theta = 1.0;	//	Falling speed of the penalty limit D
	if (params.find("-mt") != params.end())
	{
		sl_method.method  = params.find("-mt")->second;	// Metodo de seleción de individuos a usar

		if (sl_method.method == "DIV")
		{
			sl_method.sub_method = "BEST";	//	{BEST, RAND}
			if(params.find("-div_mth") != params.end())
			{
				sl_method.sub_method = params.find("-div_mth")->second;
				if (sl_method.sub_method != "RAND" && sl_method.sub_method != "BEST")
				{
					ERROR("Error! the mechanism: " + sl_method.sub_method + " is not recognized");
				}
			}

			sl_method.pop_pctg = 1.0;	// Porcentage de la poblacion del frente a poder elegir en el mecanismo aleatorio
			if (sl_method.sub_method == "RAND" && params.find("-pop_pctg") != params.end())
			{
				sl_method.pop_pctg = std::stod(params.find("-pop_pctg")->second);
				if (sl_method.pop_pctg <= 0.0 || sl_method.pop_pctg > 1.0)
				{
					ERROR("Error! -pop_pctg is out of range");
				}
			}

			sl_method.theta = 1.0;	//	Linear displacement
			if (params.find("-theta") != params.end())
			{
				sl_method.theta = std::stod(params.find("-theta")->second);
				if (sl_method.theta < 0.0 || sl_method.theta > 1.0)
				{
					ERROR("Error! -theta is out of range");
				}
			}
		}
		else if(sl_method.method != "ELITISM")
		{
			ERROR("Error! the mechanism: " + sl_method.method + " is not recognized");
		}
	}

	OUTFILE keeper;
	keeper.filename = "output.txt";
	if (params.find("-outfile") != params.end())
	{
		keeper.filename = params.find("-outfile")->second;	// Nombre del archivo para alamacenar datos
	}

	std::string path = "";
	if (params.find("-infile") != params.end())
	{
		path = params.find("-infile")->second;	// Nombre del archivo para alamacenar datos
	}

	keeper.iterations = 100;	// 	each n iterations print information
	if (params.find("-itprint") != params.end())
	{
		keeper.iterations = (double)std::stoi(params.find("-itprint")->second);	// Nombre del archivo para alamacenar datos
		if (keeper.iterations < 1) ERROR("Error! -itprint is out of range");
	}

	std::ofstream myfile;
	myfile.open (keeper.filename);
	//myfile << "#########################################################" << std::endl;
	myfile << "Parameters review " << std::endl;
	myfile << "Function: \t" << problem << std::endl;
	myfile << "Dimension: \t" << dimension << std::endl;
	myfile << "Size population: \t" << size_population << std::endl;
	myfile << "Crossover prob: \t" << CR << std::endl;
	myfile << "Lower step size: \t" << F.F_low << std::endl;
	myfile << "Upper Step size: \t" << F.F_up << std::endl;
	myfile << "Probability of choosing the F-low as step size: \t" << F.F_prb << std::endl;
	myfile << "Seed: \t" << seed << std::endl;
	myfile << "Auxiliar input path: \t" << path << std::endl;
	myfile << "Iterations: \t" << iterations << std::endl;
	myfile << "Selection method: \t" << sl_method.method << std::endl;
	myfile << "Sub selection method: \t" << sl_method.sub_method << std::endl;
	myfile << "Percentage of PF for random selection: \t" << sl_method.pop_pctg << std::endl;
	myfile << "Falling speed of the penalty limit D: \t" << sl_method.theta << std::endl;
	//myfile << "#########################################################" << std::endl;
	myfile.close();

	DE ex1(problem, dimension, size_population, seed, path);
	ex1.minimize(iterations, CR, F, sl_method, keeper);
}
