/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

#include "parameter_handler.h"

//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>

//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;
 std::map<std::string, int>  func_code;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////

void PARAMETER::insert_keys(  )
 {	
	func_code["SPHERE_FUNC"]			=	1;
	func_code["ELLIPS_FUNC"]			=	2;
	func_code["BENT_CIGAR_FUNC"]		=	3;
	func_code["DISCUS_FUNC"]			=	4;
	func_code["DIF_POWERS_FUNC"]		=	5;
	func_code["ROSENBROCK_FUNC"]		=	6;
	func_code["SCHAFFER_F7_FUNC"]		=	7;
	func_code["ACKLEY_FUNC"]			=	8;
	func_code["WEIERSTRASS_FUNC"] 		=	9;
	func_code["GRIEWANK_FUNC"]			=	10;
	func_code["RASTRIGIN_FUNC_CERO"]	=	11;
	func_code["RASTRIGIN_FUNC_UNO"]		=	12;
	func_code["STEP_RASTRIGIN_func"]	= 	13;
	func_code["SCHWEFEL_FUNC_CERO"]		= 	14;
	func_code["SCHWEFEL_FUNC_UNO"]		= 	15;
	func_code["KATSUURA_FUNC"]			=	16;
	func_code["BI_RASTRIGIN_FUNC_CERO"]	=	17;
	func_code["BI_ASTRIGIN_FUNC_UNO"]	= 	18;
	func_code["GRIE_ROSEN_FUNC"]		=	19;
	func_code["ESCAFFER6_FUNC"]			= 	20;
	func_code["CF01"] 					= 	21;
	func_code["CF02"]					= 	22;
	func_code["CF03"]					= 	23;
	func_code["CF04"]					= 	24;
	func_code["CF05"]					= 	25;
	func_code["CF06"]					= 	26;
	func_code["CF07"] 					= 	27;
	func_code["CF08"]					= 	28;
 }

std::map<std::string, std::string> PARAMETER::unpack_parameters(int argc, char *argv[])
{
	std::map<std::string, std::string> params;

	if (argc % 2 == 0)
	{
		std::cout << "Parameters error!" << std::endl;
		exit(-1);
	}

	for (int i = 1; i < argc; i+=2)
	{
		params[std::string(argv[i])] = std::string(argv[i+1]);
		//std::cout <<  << std::endl;
	}

	// show content
	//for (std::map<std::string, std::string>::iterator it=params.begin(); it!=params.end(); ++it)
    	//std::cout << it->first << " => " << it->second << '\n';

    return params;
}

int PARAMETER::decode_function_name(std::string name)
{
	return 2;
}


