# include "pareto_front.h"

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//////// Inicio de funciones genericas

/*
*	\brief		Compara la dominancia de un par de individuos
*	\return		True: A dom B False: A not dom B
*/
bool PARETO::check_dom(individual A, individual B)
{
	if (A.dcn > B.dcn && A.fx <= B.fx)	return true;
	if (A.dcn == B.dcn)
	{
		if (A.fx < B.fx) return true;
	}

	return false;
}

/*
*	
*/
std::vector< individual > PARETO::Get_Front(std::vector< individual >::iterator points_begin, std::vector< individual >::iterator points_end, double D)
{
	//show_info(points_begin, points_end);
	std::sort(points_begin, points_end, sort_by_objetives);
	//show_info(points_begin, points_end);
	//exit(-1);

	// El vector se encuentra ordenado de mayor a menor respecto al dcn, 
	// debido a esto que si el primer elemento no cumple con la condición de penalización no entra en el frente y este es vacio
	std::vector< individual > front(0);
	int id = 0;
	points_begin->id = id;
	front.push_back(*points_begin);

	if (points_begin->dcn <= D)
	{
		// retorna el frente con el elemento de mayor dcn sin importar si esta o no penalizado
		return front;
	}

	points_begin ++;

	id ++;
	while(points_begin != points_end)
	{
		int it = 0;
		points_begin->id = id;

		for (; it < (int)front.size(); ++it)
		{
			if (check_dom(front[it], *points_begin))
			{
				break;
			}
		}

		if (it >= (int)front.size())
		{
			front.push_back(*points_begin);
		}

		points_begin ++;
		id ++;
	}

	//show_info(front.begin(), front.end());
	//exit(-1);
	return front;
}

void PARETO::show_info(std::vector< individual >::iterator points_begin, std::vector< individual >::iterator points_end)
{
	/*
	std::vector<double> x;
 	double fitness = 0.0;
 	double fx;
 	double dcn;
 	int id;
	*/
 	std::cout << "fitness\tfx\tdcn\tid" << std::endl; 
	while(points_begin != points_end)
	{
		std::cout << points_begin->fitness << "\t" << points_begin->fx << "\t" << points_begin->dcn << "\t" << points_begin->id << std::endl;
		points_begin ++;	
	}

	std::cout << "\n\n" << std::endl;

}

//////// Fin de funciones genericas
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
