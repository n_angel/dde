#ifndef PARETO_FRONT_H
#define PARETO_FRONT_H

//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <iostream>
# include <climits>
# include <cstddef>
# include <algorithm> 

# include "global.h"
# include "io_structures.h"

namespace PARETO
{
	bool check_dom(individual A, individual B);
	std::vector< individual > Get_Front(std::vector< individual >::iterator points_begin, std::vector< individual >::iterator points_end, double D);
	void show_info(std::vector< individual >::iterator points_begin, std::vector< individual >::iterator points_end);
}
#endif