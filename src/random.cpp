/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

#include "random.h"

//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>

//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;


 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////

double random_0to1()
{
	return ((double) rand() / (RAND_MAX));
}