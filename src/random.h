/*
 * DifferentialEvolution.h
 *
 */


#ifndef RANDOM_H
#define RANDOM_H


//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <iostream>
# include <map>


//// Definitions/namespaces. ////////////////////////////////////////////////

double random_0to1();

# endif